-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: fly_task2
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fl_auth`
--

DROP TABLE IF EXISTS `fl_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_auth` (
  `auth_id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_pass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_date` int(11) DEFAULT NULL,
  `auth_last_logon` int(11) DEFAULT NULL,
  `auth_active` tinyint(1) DEFAULT NULL,
  `auth_activate_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_blocked` tinyint(1) DEFAULT '0',
  `auth_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacture_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT '0',
  `cart` text COLLATE utf8_unicode_ci,
  `city_id` int(11) DEFAULT NULL,
  `pass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cart_branded` text COLLATE utf8_unicode_ci,
  `funnel_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`auth_id`),
  KEY `auth_last_logon` (`auth_last_logon`),
  KEY `auth_user` (`auth_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_auth`
--

LOCK TABLES `fl_auth` WRITE;
/*!40000 ALTER TABLE `fl_auth` DISABLE KEYS */;
/*!40000 ALTER TABLE `fl_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_auth_params`
--

DROP TABLE IF EXISTS `fl_auth_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_auth_params` (
  `auth_param_id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_param_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_param_type` tinyint(4) DEFAULT NULL,
  `auth_param_format` tinyint(4) DEFAULT NULL,
  `auth_param_number` tinyint(4) DEFAULT NULL,
  `auth_param_required` tinyint(4) DEFAULT NULL,
  `auth_param_show` tinyint(4) DEFAULT NULL,
  `auth_param_uniq` tinyint(4) DEFAULT NULL,
  `auth_param_forever` tinyint(4) DEFAULT NULL,
  `auth_param_change` tinyint(4) DEFAULT NULL,
  `auth_param_ununiq_message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_param_pattern` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_param_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_param_imgw` int(11) DEFAULT NULL,
  `auth_param_imgh` int(11) DEFAULT NULL,
  `auth_param_size` int(11) DEFAULT NULL,
  PRIMARY KEY (`auth_param_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_auth_params`
--

LOCK TABLES `fl_auth_params` WRITE;
/*!40000 ALTER TABLE `fl_auth_params` DISABLE KEYS */;
INSERT INTO `fl_auth_params` VALUES (1,'Имя',1,0,1,1,0,0,0,1,'','','',NULL,NULL,NULL),(2,'Фамилия',1,0,2,1,0,0,0,1,'','','',NULL,NULL,NULL),(4,'Отчество',1,0,3,0,0,0,0,1,'','','',NULL,NULL,NULL),(5,'Дата рождения',1,0,4,0,0,0,0,1,'','','',NULL,NULL,NULL),(6,'Телефон',1,0,5,0,0,0,0,1,'','','',NULL,NULL,NULL),(7,'Добавочный',1,0,6,0,0,0,0,1,'','','',NULL,NULL,NULL);
/*!40000 ALTER TABLE `fl_auth_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_auth_params_values`
--

DROP TABLE IF EXISTS `fl_auth_params_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_auth_params_values` (
  `auth_param_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_param_id` int(11) DEFAULT NULL,
  `auth_param_text` text COLLATE utf8_unicode_ci,
  `auth_param_int` int(11) DEFAULT NULL,
  `auth_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`auth_param_value_id`),
  KEY `auth_param_id` (`auth_param_id`),
  KEY `auth_id` (`auth_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_auth_params_values`
--

LOCK TABLES `fl_auth_params_values` WRITE;
/*!40000 ALTER TABLE `fl_auth_params_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `fl_auth_params_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_company`
--

DROP TABLE IF EXISTS `fl_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacture_id` int(11) DEFAULT NULL,
  `company_active` tinyint(1) DEFAULT '0',
  `admin_id` int(11) DEFAULT '0',
  `company_options` text COLLATE utf8_unicode_ci,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_delivery` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code1c` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_inn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_kpp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_okpo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_ogrn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_okato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_post_address` text COLLATE utf8_unicode_ci,
  `contr_ur_address` text COLLATE utf8_unicode_ci,
  `contr_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_bank_nubmer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_bank_cor` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contr_bank_bik` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contacts` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`company_id`),
  KEY `manufacture_id` (`manufacture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_company`
--

LOCK TABLES `fl_company` WRITE;
/*!40000 ALTER TABLE `fl_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `fl_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fl_manufacture`
--

DROP TABLE IF EXISTS `fl_manufacture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fl_manufacture` (
  `manufacture_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacture_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacture_active` tinyint(1) DEFAULT '0',
  `city_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT '0',
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kpp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ogrn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode_fact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_fact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_fact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city_fact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_fact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_fact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_data` text COLLATE utf8_unicode_ci,
  `subdomain` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`manufacture_id`),
  UNIQUE KEY `subdomain` (`subdomain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fl_manufacture`
--

LOCK TABLES `fl_manufacture` WRITE;
/*!40000 ALTER TABLE `fl_manufacture` DISABLE KEYS */;
/*!40000 ALTER TABLE `fl_manufacture` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-09 12:35:17
