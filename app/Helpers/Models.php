<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Models
 *
 * Общие действия над моделями.
 *  ! Не для бизнес-логики
 *
 * @package App\Helpers
 */
Class Models
{
    /**
     * Заполняет заполняемые атрибуты у модели
     * @param Model $model
     * @param array $data
     */
    public static function fill(Model $model, array $data)
    {
        foreach ($model->getFillable() as $attrName) {
            if (array_key_exists($attrName, $data)) {
                $model->setAttribute($attrName, $data[$attrName]);
            }
        }
    }

    /**
     * Заполняет заполняемые атрибуты у модели и сразу сохраняет
     * @param Model $model
     * @param array $data
     * @return bool
     */
    public static function fillAndSave(Model $model, array $data)
    {
        self::fill($model, $data);
        return $model->save();
    }
}