<?php

namespace App\Helpers;

/**
 * Array cache
 *
 * Class Cache
 * @package App\Helpers
 */
Class Cache
{
    private static $instance;

    private $data = [];

    private function __construct(){}

    /**
     * @return Cache
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new self;
        }

        return static::$instance;
    }

    public function put($key, $val)
    {
        $this->data[$key] = $val;
    }

    public function get($key, $default = null)
    {
        if ($this->has($key)) {
            return $this->data[$key];
        }
        return $default;
    }

    public function has($key)
    {
        return array_key_exists($key, $this->data);
    }

    final public function __clone(){}
    final public function __wakeup(){}
}