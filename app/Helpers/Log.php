<?php

namespace App\Helpers;

/**
 * Class Log
 * @package App\Helpers
 */
Class Log
{

    /**
     * Сформировать стандартное сообщение
     * @param $method - метод, функция и тд
     * @param $file - файл, где произошла ошибка
     * @param $line - строка, где произошла ошибка
     * @param $message - сообщение об ошибке
     * @return string
     */
    public static function makeDefaultMessage($method, $file, $line, $message)
    {
        return sprintf('[%s] %s: %s Message: %s', $method, $file, $line, $message);
    }

    /**
     * Добавить в лог предупреждающее сообщение
     * @param $message
     * @param array $context
     */
    public static function warning($message, $context = [])
    {
        error_log(sprintf("[APP WARNING] %s %s", $message, json_encode($context)), 0);
    }

    /**
     * Добавить в лог информирующее сообщение
     * @param $message
     * @param array $context
     */
    public static function info($message, $context = [])
    {
        error_log(sprintf("[APP INFO] %s %s", $message, json_encode($context)), 0);
    }

    /**
     * Добавить в лог сообщение об ошибке
     * @param $message
     * @param array $context
     */
    public static function error($message, $context = [])
    {
        error_log(sprintf("[APP ERROR] %s %s", $message, json_encode($context)), 0);
    }

    /**
     * Добавить в лог сообщение для отладки
     * @param $message
     * @param array $context
     */
    public static function debug($message, $context = [])
    {
        error_log(sprintf("[APP DEBUG] %s %s", $message, json_encode($context)), 0);
    }
}