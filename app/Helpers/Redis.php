<?php

namespace App\Helpers;

use Carbon\Carbon;
use Predis\Client;

Class Redis
{
    private static $instance;

    private function __construct(){}

    /**
     * @return Client
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new \Predis\Client(null, ['prefix' => md5(APP_DIR). ':']);
        }

        return static::$instance;
    }

    final public function __clone(){}
    final public function __wakeup(){}
}