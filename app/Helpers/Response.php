<?php

namespace App\Helpers;

use App\Exceptions\FileUploadException;
use App\Exceptions\ValidationException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

/**
 * Class Response
 * @package App\Helpers
 */
Class Response
{
    /**
     * @param \Exception $e
     * @param bool $json
     * @return array|string
     */
    public static function makeByException(\Exception $e, $json = true)
    {
        if ($e instanceof \InvalidArgumentException || $e instanceof ValidationException) {
            return self::makeErrorDefault($e->getMessage(), $json);
        }

        if ($e instanceof QueryException) {
            $error = 'Произошла непредвиденная ошибка';
            if ($e->getCode() == 23000) {
                $error = 'Невозможно совершить данную операцию так как данная сущность на данный момент связана с другими сущностями';
            }
            Log::error($e->getMessage(), [$e->getTraceAsString()]);
            return self::makeErrorDefault($error, $json);
        }

        if ($e instanceof FileUploadException) {
            Log::error($e->getMessage(), ['code' => $e->getCode(), 'trace' => $e->getTraceAsString()]);
            return self::makeErrorDefault($e->getMessage() . ' #' . $e->getCode(), $json);
        }

        if ($e instanceof \Exception) {
            Log::error($e->getMessage(), [$e->getTraceAsString()]);
            return self::makeErrorDefault('Произошла непредвиденная ошибка', $json);
        }

        Log::error($e->getMessage(), [get_class($e), $e->getTraceAsString()]);
        return self::makeErrorDefault('Не удалось обработать исключение', $json);
    }

    /**
     * @param $content
     * @param bool $json
     * @return array|string
     */
    public static function makeContent($content, $json = true)
    {
        $response = [
            'result' => true,
            'content' => $content,
        ];

        if ($json) {
            return json_encode($response);
        }

        return $response;
    }

    /**
     * @param string $message
     * @param bool $json
     * @return array|string
     */
   public static function makeErrorDefault($message, $json = true) {
        $response = [
            'error' => true,
            'errorMessage' => $message,
        ];

        if ($json) {
            return json_encode($response);
        }

       return $response;
   }

    /**
     * @param string $message
     * @param bool $json
     * @return array|string
     */
   public static function makeSuccessDefault($message = '', $json = true) {
        $response = [
            'result' => true,
        ];

        if ($message) {
            $response['message'] = $message;
        }

        if ($json) {
            return json_encode($response);
        }

       return $response;
   }

    /**
     * @param Collection $data
     * @param bool $json
     * @return array|string
     */
   public static function makeByCollection(Collection $data, $json = true)
   {
       if ($json) {
           return $data->toJson();
       }
       return $data->toArray();
   }

}