<?php

namespace App\Helpers;

/**
 * Class Blade
 *
 * Экземпляр шаблонизатора
 * Получить можно через функцию view()
 *  ! Не использовать напрямую, тк в фреймворке его не будет, зато будет функция view()
 *
 * @package App\Helpers
 *
 */
Class Blade
{
    private static $instance;

    private function __construct(){}

    /**
     * @return \Philo\Blade\Blade
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new \Philo\Blade\Blade(TEMPLATE_DIR, TEMPLATE_CACHE_DIR);
        }

        return static::$instance;
    }
    final public function __clone(){}
    final public function __wakeup(){}
}