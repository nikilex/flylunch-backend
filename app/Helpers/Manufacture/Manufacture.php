<?php

namespace App\Helpers\Manufacture;

use App\Exceptions\ValidationException;
use App\Helpers\Models;
use App\Models\Firelunch\Auth as AuthModel;
use App\Models\Firelunch\Manufacture as ManufactureModel;
use Illuminate\Support\Collection;
use App\Helpers\SetOfDish\SetOfDishType as SetOfDishTypeHelper;
use App\Models\Flylunch\ManufactureSetOfDishType as ManufactureSetOfDishTypeModel;
use Illuminate\Support\Facades\DB;

Class Manufacture
{
    public static function getAccounts($manufacture_id)
    {
        return ManufactureModel
            ::with(['users'])
            ->where('manufacture_id', '<>', $manufacture_id)
            ->orderBy('manufacture_name')
            ->get();
    }

    public static function getById($id)
    {
        return ManufactureModel::isActive()->find($id);
    }

    public static function getBySubdomain($subdomain)
    {
        return ManufactureModel::where('subdomain', $subdomain)->first();
    }

    /**
     * Добавление производства из админки
     *
     * todo: перенести добавление из www/components/admin/admin_manufactureadd.php
     *
     * @param Collection $data
     */
    public static function post(Collection $data)
    {
        $m = $data->get('manufacture');

        // Подключить меню
        foreach (SetOfDishTypeHelper::getFreeTypes() as $type) {
            Models::fillAndSave(new ManufactureSetOfDishTypeModel(), [
                'manufacture_id' => $m->getKey(),
                'set_of_dish_type_id' => $type->getKey(),
            ]);
        }
    }

    /**
     * Удаление производства
     *
     * todo: Сделано побыстрому. На будущее - надо уточнить логику удаления производства, с какими связями удалять можно, с какими нельзя и доделать правильное удаление
     *
     * @param AuthModel $user
     * @param $manufacture_id
     * @return Collection
     * @throws ValidationException
     */
    public static function delete(AuthModel $user, $manufacture_id)
    {
        if (!$user->isSuperAdmin) {
            throw new ValidationException('У вас недостаточно прав для совершения данной операции');
        }

        $manufacture = ManufactureModel::find($manufacture_id);

        if (!$manufacture) {
            throw new \InvalidArgumentException('Производство не найдено');
        }

        if ($manufacture->getKey() == $user->manufacture_id) {
            throw new ValidationException('Нельзя удалить свое производство');
        }

        DB::beginTransaction();
        $manufacture->forceDeleteWithRelationship();
        DB::commit();

        return collect([
            'result' => true
        ]);
    }

     /**
     * Добавление производства из формы регистрации
     * @param App\Helpers\Request $request
     * @return array $manufacture
     */
    public static function registration($request)
    {
        $manufacture = ManufactureModel::create($request)->toArray();
        $admin = AuthModel::create([
            'auth_user'      => $request['email'],
            'auth_pass'      => md5($request['password']),
            'auth_type'      => 'admin',
            'pass'           => $request['password'],
            'manufacture_id' => $manufacture['manufacture_id']
        ]);
       
        return $manufacture;
    }

    /**
     * Валидация основных данных производства (для post/put)
     * @param Collection $data
     * @throws ValidationException
     */
    public static function validate(Collection $data)
    {
        if (!$data->get('subdomain')) {
            throw new ValidationException('Укажите поддомен');
        }

        /** todo: сделать поприличнее */
        if ($data->get('subdomain') == 'pl' || $data->get('subdomain') == 'dev') {
            throw new ValidationException('Поддомен занят');
        }

        if ($data->get('manufacture') instanceof ManufactureModel) {
            $m = ManufactureModel
                ::where('subdomain', $data->get('subdomain'))
                ->where('manufacture_id', '<>', $data->get('manufacture')->getKey())
                ->first();
            if ($m) {
                throw new ValidationException('Поддомен занят');
            }
        } else {
            $m = ManufactureModel::where('subdomain', $data->get('subdomain'))->first();
            if ($m) {
                throw new ValidationException('Поддомен занят');
            }
        }
    }
}