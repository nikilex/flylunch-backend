<?php

namespace App\Helpers;

/**
 * Class Request
 *
 * Входящие данные на сервер
 * Экземпляр можно получить при помощи функции request()
 * ! Не использовать напрямую, тк в фреймворке его не будет.
 *
 * @package App\Helpers
 */
Class Request
{
    private static $instance;

    private function __construct(){}

    public static function getInstance()
    {
        global $GLOBAL_AUTH;
        if (is_null(static::$instance)) {
            static::$instance = new self;
        }

        return static::$instance;
    }
    final public function __clone(){}
    final public function __wakeup(){}

    /**
     * Получить все переменные
     * @return array|mixed
     */
    public function all()
    {
        global $_V;
        return !empty($_V) ? $_V : $_REQUEST;
    }

    /**
     * Получить переменную по ключу
     * @param null $key
     * @param null $default
     * @return null
     */
    public function get($key = null, $default = null)
    {
        global $_V;
        if(empty($_V)) $_V = $_REQUEST;
        if (array_key_exists($key, $_V)) {
            return $_V[$key];
        }
        if (!empty($default)) {
            return $default;
        }
        return null;
    }

    /**
     * Проверить существование переменной
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        global $_V;
        return array_key_exists($key, $_V);
    }
}