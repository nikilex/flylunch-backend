<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;

/**
 * Class Functions
 *
 * Общие функции
 *
 * @package App\Helpers
 */
Class Functions
{

    /**
     * Получить список дней или название дня недели.
     * Номера дней в соответствии со стандартом ISO-8601
     *
     * @param null $day_of_week
     * @return array|mixed|string
     */
    public static function getWeekDays($day_of_week = null)
    {
        $days = [
            1 => 'Понедельник',
            2 => 'Вторник',
            3 => 'Среда',
            4 => 'Четверг',
            5 => 'Пятница',
            6 => 'Суббота',
            7 => 'Воскресенье',
        ];

        if ($day_of_week) {
            return isset($days[$day_of_week]) ? $days[$day_of_week] : 'Неизвестно';
        }

        return $days;
    }

    /**
     * Проверяет, доступен ли еще заказ на дату $u и время доставки $deliveryTime
     * @param $u
     * @param $deliveryTime
     * @return bool
     *
     * @author спизжено отсюда www/components/order/_common.php
     */
    public static function isDeliveryTimeAvail($u, $deliveryTime) {
        $today = strtotime('now 00:00:00');
        if ($today > $u) {
            // сегодняшний день уже больше, чем проверяемая дата - нельзя заказать
            return false;
        } else {
            if (self::isDeliveryTimeExceed($deliveryTime)) {
                // время доставку уже недоступно для заказа
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * истекло ли время для заказа на утро/день/вечер/ночь
     * @param $deliveryTime
     * @return bool
     */
    public static function isDeliveryTimeExceed($deliveryTime) {
        $result = false;

        if ($deliveryTime == 1) {
            if (date('H') >= 11) {
                $result = true;
            }
        }
        if ($deliveryTime == 2) {
            if (date('H') >= 15) {
                $result = true;
            }
        }
        if ($deliveryTime == 3) {
            if (date('H') >= 21) {
                $result = true;
            }
        }
        if ($deliveryTime == 4) {
            if (date('H') >= 21) {
                $result = true;
            }
        }
        // заглушка
        $result = false;

        return $result;
    }

    /**
     * Дебаг SQL запросов
     */
    public static function DebugDBQueryLog()
    {
        DB::enableQueryLog();
        register_shutdown_function(function() {
            echo "<pre>";
            print_r(DB::getQueryLog());
        });
    }

    /**
     * Разделение тысячных у сумм
     * @param $var
     * @param bool $decimals
     * @return string
     */
    public static function priceFormat($var, $decimals = false)
    {
        if ($decimals === false) {
            // не указано кол-во дробных знаков: если есть копейки - показываем, если нет, то нет
            return number_format($var, 2, '.', ' ');
        }
        return number_format($var, $decimals, '.', ' ');
    }

    /**
     * Готовит строку с числом к дальнейшему использованию
     * (Удалет из строки пробелы, заменяет запятые и тд)
     * @param $var
     * @return mixed
     */
    public static function normalizeNumber($var)
    {
        if (!trim($var)) {
            return 0;
        }
        return str_replace([' ', ','], ['', '.'], $var);
    }

    public static function calculateWithPercent($weight, $percent, $decimals = 0)
    {
        $percent = (float)$percent;
        if ($percent <= 0 || !is_numeric($weight)) {
            return 0;
        }
        return round($weight/((100-$percent)/100), $decimals);
    }

    public static function checkPassword($password)
    {
        return ! preg_match('/[а-яА-ЯёЁ\s]/u', $password);
    }
}