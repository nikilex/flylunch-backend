<?php

namespace App\Helpers;

/**
 * Class Auth
 *
 * Временная замена класса Auth фреймворка.
 * Получить экзепляр класса можно через функцию user()
 *  ! Не использовать напрямую из-за того что потом придется рефакторить, тк в фреймворке будет другое пространство имен
 *
 * @package App\Helpers
 */
Class Auth
{
    private static $instance;

    private function __construct(){}

    /**
     * @return \App\Models\Firelunch\Auth
     */
    public static function getInstance()
    {
        global $GLOBAL_AUTH;
        if (is_null(static::$instance)) {
            if (!empty($GLOBAL_AUTH['id']) && ($auth = \App\Models\Firelunch\Auth::find($GLOBAL_AUTH['id']))) {
                static::$instance = $auth;
            }
        }

        return static::$instance;
    }
    final public function __clone(){}
    final public function __wakeup(){}
}