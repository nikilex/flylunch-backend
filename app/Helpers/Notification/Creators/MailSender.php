<?php

namespace App\Helpers\Notification\Creators;

use App\Helpers\Notification\NotificationSender;
use App\Helpers\Notification\Interfaces\NotificationConnector;
use App\Helpers\Notification\Connectors\MailConnector;

/**
 * Этот Конкретный Создатель поддерживает Facebook. Помните, что этот класс
 * также наследует метод post от родительского класса. Конкретные Создатели —
 * это классы, которые фактически использует Клиент.
 */
class MailSender extends NotificationSender
{
    public function getSendMethod(): NotificationConnector
    {
        return new MailConnector();
    }
}

?>