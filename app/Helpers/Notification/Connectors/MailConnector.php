<?php

namespace App\Helpers\Notification\Connectors;

use App\Helpers\Notification\Interfaces\NotificationConnector;
use PHPMailer\PHPMailer\PHPMailer;

class MailConnector implements NotificationConnector
{
    private $subject;
    private $altBody;

    public function __construct(string $subject = '', string $altBody = '')
    {
        $this->subject = $subject;
        $this->altBody = $altBody;
    }

    public function send($content): void
    {
        //Create a new PHPMailer instance
        $mail = new PHPMailer;
        //Set who the message is to be sent from
        $mail->setFrom(MAIL_FROM, MAIL_FROM_NAME);
        //Set who the message is to be sent to
        $mail->addAddress(MAIL_TO, MAIL_TO_NAME);
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($content);
        //Set the subject line
        $mail->Subject = $this->subject;
        //Replace the plain text body with one created manually
        $mail->AltBody = $this->altBody;

        //send the message, check for errors
        if (!$mail->send()) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message sent!';
        }
    }
}

?>