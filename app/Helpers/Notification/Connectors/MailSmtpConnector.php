<?php

namespace App\Helpers\Notification\Connectors;

use App\Helpers\Notification\Interfaces\NotificationConnector;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class MailSmtpConnector implements NotificationConnector
{
    private $subject;
    private $altBody;

    public function __construct(string $subject = '', string $altBody)
    {
        $this->subject = $subject;
        $this->altBody = $altBody;
    }

    public function send($content): void
    {
        //Create a new PHPMailer instance
        $mail = new PHPMailer;
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // SMTP::DEBUG_OFF = off (for production use)
        // SMTP::DEBUG_CLIENT = client messages
        // SMTP::DEBUG_SERVER = client and server messages
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;
        //Set the hostname of the mail server
        $mail->Host = SMTP_HOST;
        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port = SMTP_PORT;
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication
        $mail->Username = SMTP_USER;
        //Password to use for SMTP authentication
        $mail->Password = SMTP_PASSWORD;
        //Set who the message is to be sent from
        $mail->setFrom(MAIL_FROM, MAIL_FROM_NAME);
        //Set an alternative reply-to address
        $mail->addReplyTo(MAIL_TO, MAIL_TO_NAME);
        //Set who the message is to be sent to
        $mail->addAddress(MAIL_TO, MAIL_TO_NAME);
        //Set the subject line
        $mail->Subject = $this->subject;
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($content);
        //Replace the plain text body with one created manually
        $mail->AltBody = $this->altBody;

        //send the message, check for errors
        if (!$mail->send()) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message sent!';
        }
    }
}
