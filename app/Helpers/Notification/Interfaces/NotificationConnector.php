<?php

namespace App\Helpers\Notification\Interfaces;
/**
 * Интерфейс Продукта объявляет поведения различных типов продуктов.
 */
interface NotificationConnector
{
    public function send($content): void;
}
