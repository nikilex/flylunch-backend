<?php

namespace App\Helpers\Notification;

use App\Helpers\Notification\Interfaces\NotificationConnector;

abstract class NotificationSender
{
    /**
     * Фактический фабричный метод. Обратите внимание, что он возвращает
     * абстрактный коннектор. Это позволяет подклассам возвращать любые
     * конкретные коннекторы без нарушения контракта суперкласса.
     */
    abstract public function getSendMethod(): NotificationConnector;

    /**
     * Когда фабричный метод используется внутри бизнес-логики Создателя,
     * подклассы могут изменять логику косвенно, возвращая из фабричного метода
     * различные типы коннекторов.
     */
    public function send($content): void
    {
        // Вызываем фабричный метод для создания объекта Продукта...
        $network = $this->getSendMethod();
        // ...а затем используем его по своему усмотрению.
        $network->send($content);
    }
}

?>