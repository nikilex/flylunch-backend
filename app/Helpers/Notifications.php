<?php

namespace App\Helpers;

use App\Helpers\Notification\NotificationSender;

/**
 * Class Notification
 *
 * Уведомления.
 *
 * @package App\Helpers
 */
class Notifications
{
    public static function send(NotificationSender $creator, string $message)
    {
        $creator->send($message);
    }   
}
