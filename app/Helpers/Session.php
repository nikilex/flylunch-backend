<?php

namespace App\Helpers;

Class Session
{
    public static function put($key, $val)
    {
        $_SESSION[$key] = $val;
    }

    public static function get($key)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public static function getSelectedSetOfDishClient()
    {
        return self::get('set_of_dish_client');
    }
    public static function putSelectedSetOfDishClient($id)
    {
        self::put('set_of_dish_client', $id);
    }

    public static function getSelectedCmnMenuWeek()
    {
        return self::get('cmn_menu_week_id');
    }
    public static function putSelectedCmnMenuWeek($id)
    {
        self::put('cmn_menu_week_id', $id);
    }

    public static function getSelectedCmnMenuType()
    {
        return self::get('cmn_menu_m_set_of_dish_type_id');
    }
    public static function putSelectedCmnMenuType($id)
    {
        self::put('cmn_menu_m_set_of_dish_type_id', $id);
    }

    public static function getSelectedCmnMenuDay()
    {
        return self::get('cmn_menu_m_set_of_dish_day');
    }
    public static function putSelectedCmnMenuDay($id)
    {
        self::put('cmn_menu_m_set_of_dish_day', $id);
    }

    public static function getSelectedCookingMenuType()
    {
        return self::get('cooking_menu_m_set_of_dish_type_id');
    }
    public static function putSelectedCookingMenuType($id)
    {
        self::put('cooking_menu_m_set_of_dish_type_id', $id);
    }

    public static function getSelectedCookingMenuWeek()
    {
        return self::get('cooking_menu_week_id');
    }
    public static function putSelectedCookingMenuWeek($id)
    {
        self::put('cooking_menu_week_id', $id);
    }

    public static function getSelectedCookingMenuCategory()
    {
        return self::get('cooking_menu_category_id');
    }
    public static function putSelectedCookingMenuCategory($id)
    {
        self::put('cooking_menu_category_id', $id);
    }
}