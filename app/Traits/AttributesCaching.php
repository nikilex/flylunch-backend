<?php

namespace App\Traits;

Trait AttributesCaching
{
    public function __get($key)
    {
        if (!array_key_exists($key, $this->attributes)) {
            $this->setAttribute($key, $this->getAttribute($key));
        }
        return $this->attributes[$key];
    }
}