<?php

namespace App\Models\Firelunch;

use App\Models\Flylunch\DeliveryTime;
use Illuminate\Database\Eloquent\Model;

Class Company extends  Model
{
    protected $table = PFX.'company';
    protected $primaryKey = 'company_id';
    public $timestamps = false;

    protected $fillable = [
        'manufacture_id',
        'company_active',
        'admin_id',
        'company_options',
        'company_name',
        'company_delivery',
        'company_payment',
        'code1c',
        'contr_comment',
        'contr_inn',
        'contr_kpp',
        'contr_okpo',
        'contr_ogrn',
        'contr_okato',
        'contr_post_address',
        'contr_ur_address',
        'contr_phone',
        'contr_email',
        'contr_bank_name',
        'contr_bank_nubmer',
        'contr_bank_cor',
        'contr_bank_bik',
        'contacts',
    ];

    public function manufacture()
    {
        return $this->hasOne(Manufacture::class, 'manufacture_id', 'manufacture_id');
    }

    /**
     * Список опций
     * todo: в будущем надо перенести в отдельную таблицу
     *
     * @return \Illuminate\Support\Collection
     */
    public function getOptionsAttribute()
    {
        $list = collect();
        $array = @json_decode($this->company_options, true);
        if (is_array($array)) {
            foreach ($array as $item) {
                if (!isset($item['optionValues']) || !is_array($item['optionValues'])) {
                    continue;
                }
                $item['optionValues'] = collect($item['optionValues']);
                $list->push(collect($item));
            }
        }
        return $list;
    }

    /**
     * Список типов оплаты
     * todo: в будущем надо перенести в отдельную таблицу
     *
     * @return \Illuminate\Support\Collection
     */
    public function getPaymentTypesAttribute()
    {
        $list = collect();
        $array = @json_decode($this->company_payment, true);
        if (is_array($array)) {
            foreach ($array as $index => $val) {
                $list->put($val, $val);
            }
        }
        return $list;
    }

    /**
     * Список вариантов времени доставки
     * Внимание! На данный момент варианты хранятся в таблице @see DeliveryTime
     *  поэтому важно соблюдать совместимость по id
     *
     * todo: в будущем надо перенести в отдельную таблицу
     *
     * @return \Illuminate\Support\Collection
     */
    public function getDeliveryTimeVariantsAttribute()
    {
        $list = collect();
        $array = @json_decode($this->company_delivery, true);
        if (is_array($array)) {
            foreach ($array as $index => $id) {
                $list->put($id, $id);
            }
        }
        return $list;
    }

    /**
     * Скоуп для выбора только активных компаний
     * @param $q
     * @return mixed
     */
    public function scopeIsActive($q)
    {
        return $q->where('company_active', 1);
    }

}