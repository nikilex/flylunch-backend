<?php

namespace App\Models\Firelunch;

/**
 * Class Manufacture
 * @package App\Models\Firelunch
 */
Class Manufacture extends \Illuminate\Database\Eloquent\Model
{
    protected $table = PFX.'manufacture';
    protected $primaryKey = 'manufacture_id';
    public $timestamps = false;

    protected $fillable = [
        'subdomain',
        'manufacture_name',
        'manufacture_active',
        'city_id',
        'admin_id',
        'ip',
        'inn',
        'kpp',
        'ogrn',
        'country',
        'state',
        'city',
        'street',
        'phone',
        'postcode',
        'postcode_fact',
        'country_fact',
        'state_fact',
        'city_fact',
        'street_fact',
        'phone_fact',
        'bank_data',
    ];

    public function users()
    {
        return $this->hasMany(Auth::class, 'manufacture_id', 'manufacture_id');
    }

    /**
     * Список клиентов производства
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clients()
    {
        return $this->hasMany(Company::class, 'manufacture_id', 'manufacture_id');
    }

    /**
     * Скоуп для выбора только активных производств
     * @param $q
     * @return mixed
     */
    public function scopeIsActive($q)
    {
        return $q->where('manufacture_active', 1);
    }

    public function forceDeleteWithRelationship()
    {
        foreach ($this->manufactureSetOfDishTypes as $item) {
            $item->forceDeleteWithRelationship();
        }

        $this->forceDelete();
    }
}