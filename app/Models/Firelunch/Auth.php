<?php

namespace App\Models\Firelunch;

use App\Helpers\Cache;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\Order\Order as OrderHelper;

Class Auth extends Model
{
    const ROLE_SUPERADMIN = 'superadministartor';
    const ROLE_ADMIN = 'admin';
    const ROLE_PACKER = 'packer';
    const ROLE_COURIER = 'courier';
    const ROLE_COOKING = 'cooking';
    const ROLE_MANAGER = 'manager';

    protected $table = PFX.'auth';
    protected $primaryKey = 'auth_id';
    public $timestamps = false;

    protected $hidden = [
        'auth_pass'
    ];

    protected $fillable = [
        'auth_user',
        'auth_pass',
        'auth_date',
        'auth_last_logon',
        'auth_active',
        'auth_activate_code',
        'auth_blocked',
        'auth_type',
        'manufacture_id',
        'company_id',
        'admin_id',
        'cart',
        'city_id',
        'pass',
        'cart_branded',
        'funnel_id',
    ];

    /**
     * Суперадмин
     * @return bool
     */
    public function getIsSuperAdminAttribute()
    {
        return $this->auth_type == self::ROLE_SUPERADMIN;
    }

    /**
     * Админ (обычно это админ производства)
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->auth_type == self::ROLE_ADMIN;
    }

    public function getIsPackerAttribute()
    {
        return $this->auth_type == self::ROLE_PACKER;
    }

    public function getIsCourierAttribute()
    {
        return $this->auth_type == self::ROLE_COURIER;
    }

    public function getIsCookingAttribute()
    {
        return $this->auth_type == self::ROLE_COOKING;
    }

    public function getIsManagerAttribute()
    {
        return $this->auth_type == self::ROLE_MANAGER;
    }

    /**
     * Производство
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function manufacture()
    {
        return $this->hasOne(Manufacture::class, 'manufacture_id', 'manufacture_id');
    }

    /**
     * Количество позиций в корзине
     * @return int
     */
    public function getCartCountAttribute()
    {
        $key = "user_cart_count_{$this->getKey()}";
        if(!Cache::getInstance()->has($key)) {
            Cache::getInstance()->put($key, OrderHelper::getUserCartCount($this->getKey()));
        }

        return (int)Cache::getInstance()->get($key);
    }

    /**
     * Компания пользователя
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class, 'company_id', 'company_id');
    }

    public function scopeIsActive($q)
    {
        return $q->where('auth_active', 1)->where('auth_blocked', 0);
    }
}