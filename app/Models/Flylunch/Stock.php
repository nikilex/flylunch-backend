<?php

namespace App\Models\Flylunch;

use Illuminate\Database\Eloquent\Model;

Class Stock extends Model
{
    protected $table = FLY_PFX.'stock';
    protected $primaryKey = 'stock_id';
    protected $fillable = [
        'manufacture_id',
        'product_id',
        'amount',
        'prime_cost',
    ];

    public $timestamps = false;
}