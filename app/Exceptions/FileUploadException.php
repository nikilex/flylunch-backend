<?php

namespace App\Exceptions;

/**
 * Class FileUploadException
 * @package App\Exceptions
 */
Class FileUploadException extends \Exception
{
    const CODE_PFX = 'FUE#';

    protected $error_code;

    protected $debug;

    protected $codes = [
        1 => 'Указанный файл отсутствует в директории для загрузок',
        2 => 'Нет доступа для записи в директорию хранения файлов или директория не существует',
        3 => 'Не удалось сжать и переместит изображение',
        4 => 'Пустой идентификатор у сущности',
    ];

    public function __construct(
        $message = "",
        $code = 0,
        $debug = [],
        \Throwable $previous = null
    ) {
        if ($code) {
            $this->error_code = self::CODE_PFX . $code;
        }
        if (is_array($debug)) {
            $this->debug = print_r($debug, true);
        }
        parent::__construct($message, $code, $previous);
    }

    public function getErrorCode()
    {
        return $this->error_code;
    }

    public function getCodeDescription()
    {
        return isset($this->codes[$this->original_code]) ?
            $this->codes[$this->original_code] : null;
    }

    public function getDebug()
    {
        return $this->debug;
    }
}