<?php

namespace App\Exceptions;

Class ValidationException extends \Exception
{
    public function __construct(
        $message = "",
        $code = 0,
        $debug = [],
        \Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}