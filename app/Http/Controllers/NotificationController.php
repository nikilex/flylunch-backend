<?php

namespace App\Http\Controllers;

use App\Helpers\Notifications;
use App\Helpers\Notification\Creators\MailSender;
use App\Helpers\Notification\Creators\MailSmtpSender;

Class NotificationController
{
    public function mail()
    {
        Notifications::send(new MailSender(), "Hello World");
    }

    public function smtp()
    {
        Notifications::send(new MailSmtpSender(), "Hello World");
    }
}