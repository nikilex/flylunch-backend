<?php

namespace App\Http\Controllers;

use App\Helpers\Manufacture\Manufacture;


Class RegistrationController
{
    public function index()
    {
        return view('registration');
    }

    public function store()
    {
        $request = request()->all();
        // Здесь должна быть валидация $request
        $manufacture = Manufacture::registration($request);
        return view('success', compact('manufacture'));
    }
}