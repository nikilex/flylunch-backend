<?php

session_start();

function init_app() {
    require_once APP_DIR . '/vendor/autoload.php';
    $capsule = new Illuminate\Database\Capsule\Manager;
    $capsule->addConnection([
        "driver" => "mysql",
        "host" =>DB_HOST,
        "database" => DB_NAME,
        "username" => DB_LOGIN,
        "password" => DB_PASS,
        'charset'   => 'utf8',
    ]);
    $capsule->setEventDispatcher(new \Illuminate\Events\Dispatcher(new \Illuminate\Container\Container()));
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    \Illuminate\Support\Facades\DB::swap($capsule->getDatabaseManager());
}

/**
 * Получить скомпилированный экземпляр указанного представления из шаблонизатора
 *  (сделано через функцию для будущей совместимости с фреймворком)
 *
 * @param  string  $view
 * @param  array   $data
 *
 * @return \Illuminate\View\View
 */
function view()
{
    return call_user_func_array([\App\Helpers\Blade::getInstance()->view(), 'make'], func_get_args());
}

/**
 * Получить текущего авторизованного пользователя
 *  (сделано через функцию для будущей совместимости с фреймворком)
 *
 * @return \App\Models\Firelunch\Auth
 */
function user()
{
    return \App\Helpers\Auth::getInstance();
}

/**
 * Редирект
 * @param $location
 */
function redirect($location)
{
    header('Location: ' . $location);
    die;
}

/**
 * Получить входные данные (пока минимумальный функционал, но для совместимости)
 * @param null $key
 * @param null $default
 * @return \App\Helpers\Request | mixed
 */
function request()
{
    return \App\Helpers\Request::getInstance();
}

/**
 * Экзепляр клиента Redis
 * @return \Predis\Client
 */
function cache()
{
    return \App\Helpers\Redis::getInstance();
}

try {
    init_app();
} catch (\Exception $e) {
    error_log(__FILE__ . ' ' . __LINE__ . ' [Exception] ' . $e->getMessage());
    header("HTTP/1.1 500 Internal Server Error");
    die('<h1>Internal Server Error</h1>');
}