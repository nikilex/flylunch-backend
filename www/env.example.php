<?php

define("APP_DIR", "/home/web/vm-1af2487f.na4u.ru");

define("DB_NAME", "flylunch");
define("DB_HOST", "localhost");
define("DB_LOGIN", "login");
define("DB_PASS", "pwd");

define("DADATA_API_KEY", "");

define('ENV_DOMAIN_LEVEL_3', 'pl');

define("PFX", "fl_");
define("FLY_PFX", "fly_");

// Notifications

// Mail
define("MAIL_FROM", "exampleFrom@example.com");
define("MAIL_FROM_NAME", "Alex Nik");
define("MAIL_TO", "exampleTo@example.com");
define("MAIL_TO_NAME", "John Dow");

//SMTP
define("SMTP_HOST", "smtp.gmail.com");
define("SMTP_PORT", "587");
define("SMTP_USER", "example");
define("SMTP_PASSWORD", "example");