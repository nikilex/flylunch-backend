<?php

if (!file_exists(__DIR__ . '/env.php')) {
    die('env file not found');
}

require_once __DIR__ . '/env.php';

date_default_timezone_set("Europe/Moscow");

define("ROOT_DIR", APP_DIR . "/www");
define("IMAGES_DIR", APP_DIR . "/www/images");
define('TEMPLATE_DIR', APP_DIR . '/resources/views');
define('TEMPLATE_CACHE_DIR', APP_DIR . '/storage/tpl_cache');
define("SITE_URL", "http://{$_SERVER['HTTP_HOST']}");
define("IMAGES_URL", "http://{$_SERVER['HTTP_HOST']}/images");
define("REQUEST_SCHEME", isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'https');

if (empty($_SERVER['HTTP_HOST'])) {
    header('HTTP/1.0 403 Forbidden');
    die;
}

/**
 * Пример:
 * flylunch.ru = лендинг
 * stolovaya.flylunch.ru = меню для физ лиц (анонимный доступ)
 * pl.flylunch.ru = напрямую сюда не должны заходить, но если зашли - просить ввести поддомен
 * stolovaya.pl.flylunch.ru = авторизация и админка для производств или меню, если это клиент производства
 */
$parts = array_reverse(explode('.', $_SERVER['HTTP_HOST']));

define("DOMAIN_LEVEL_1", @$parts[0]);
define("DOMAIN_LEVEL_2", @$parts[1]);
define("DOMAIN_LEVEL_3", @$parts[2]);
define("DOMAIN_LEVEL_4", @$parts[3]);

if (!DOMAIN_LEVEL_3 || (DOMAIN_LEVEL_3 == 'dev' && !DOMAIN_LEVEL_4)) {
    define('IS_LANDING', 1);
} else {
    if (!DOMAIN_LEVEL_4) {
        if (DOMAIN_LEVEL_3 == 'pl' || DOMAIN_LEVEL_3 == 'dev') {
            define('IS_ENTER_SUBDOMAIN', 1);
        } else {
            define('IS_FL_MENU', 1);
        }
    } else {
        if (DOMAIN_LEVEL_3 == 'pl' || DOMAIN_LEVEL_3 == 'dev') {
            define('IS_UR_ADMIN_PANEL', 1);
            define('CLIENT_SUBDOMAIN', DOMAIN_LEVEL_4);
        } else {
            header('HTTP/1.0 403 Forbidden');
            die;
        }
    }
}
define("COOKIE_DOMAIN", $_SERVER['HTTP_HOST']);

unset($parts);

$PFX = "fl_";

?>